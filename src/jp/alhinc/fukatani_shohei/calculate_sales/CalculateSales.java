package jp.alhinc.fukatani_shohei.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {
		LinkedHashMap<String, String> map = new LinkedHashMap<>();
		LinkedHashMap<String, Long> sale = new LinkedHashMap<>();
		List<Integer> list = new ArrayList<>();
		FilenameFilter filter = new FilenameFilter() {
			public boolean accept(File file, String str) {
				return new File(file,str).isFile() && str.matches("\\d{8}.rcd$");
			}
		};
		BufferedReader br = null;
		try {
			File file = new File(args[0],"branch.lst");

			if(file.exists()) {
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);

				String line;
				while ((line = br.readLine()) != null) {

					if(line.contains(",")) {
						String[] words = line.split(",");
						Integer number = words.length;

						if(words[0].matches("\\d{3}") && number == 2) {
							map.put(words[0], words[1]);
							sale.put(words[0], 9l);
						} else {
							System.out.println("支店定義ファイルのフォーマットが不正です");
							return;
						}
					}else {
						System.out.println("支店定義ファイルのフォーマットが不正です");
						return;
					}
				}
			} else {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("closeできませんでした。");
				}
			}
		}
		try {
			File file = new File(args[0]);
			File files[] = file.listFiles(filter);
			for (int n=0; n<files.length; n++) {
				list.add(Integer.parseInt(files[n].getName().substring(0,8)));
			}
			Collections.sort(list);
			int min = list.get(0);
			int max = list.get(files.length - 1);
			int number = files.length - 1;
			if(number == max-min ){
				for (int i=0; i<files.length; i++) {
					FileReader fr = new FileReader(files[i]);
					br = new BufferedReader(fr);
					String cd = br.readLine();
					Long price = Long.parseLong(br.readLine());
					String err = br.readLine();
					if(err==null) {
						for (String key : sale.keySet()) {
						if(sale.containsKey(cd)) {
							Long total = price + sale.get(key);
							String totalstr = String.valueOf(total);
							if(!(totalstr.matches("[0-9]{10,}"))) {
								sale.put(cd, total);
							} else {
								System.out.println("合計金額が10桁を超えました");
								return;
							}
						} else {
							System.out.println(files[i] + "の支店コードは不正です。");
							return;
						}
						}
					} else {
						System.out.println(files[i] + "のフォーマットが不正です");
						return;
					}
				}
			} else {
				System.out.println("売上ファイル名が連番になっていません");
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("closeできませんでした。");
				}
			}
		}
		try {
			File file = new File(args[0],"branch.out");
			FileWriter fw = new FileWriter(file);
			BufferedWriter bw = new BufferedWriter(fw);

			for(Map.Entry<String,String>entry : map.entrySet()) {

				String key = entry.getKey() ;
				Long value = sale.get(key);
				bw.write(entry.getKey() + "," + entry.getValue() + "," + value + "\r\n");
			}
			bw.close();
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("closeできませんでした。");
				}
			}
		}
	}
}
